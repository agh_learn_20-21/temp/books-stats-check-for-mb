import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class Parser {

    public List<Book> parse(String pathToFile) {
        File in = new File(pathToFile);
        List<Book> books = new LinkedList<>();
        String line = "";
        BufferedReader inReader = null;
        try {
            inReader = new BufferedReader(new FileReader(in));
            int rawCounter = 1;
            String author = "";
            String title = "";
            String category = "";

            int lineId = 0;
            while ((line = inReader.readLine()) != null) {
                try {
                    String lineReadyToUse = line.trim();
                    lineId++;

                    if (rawCounter == 1) {
                        if (lineReadyToUse.isEmpty()) {
                            throw new EmptyLineException("Empty line when author was parsing --> line in file: " + lineId);
                        }
                        author = lineReadyToUse;
                    } else if (rawCounter == 2) {
                        if (lineReadyToUse.isEmpty()) {
                            throw new EmptyLineException("Empty line when title was parsing --> line in file: " + lineId);
                        }
                        title = lineReadyToUse;
                    } else if (rawCounter == 3) {
                        if (lineReadyToUse.isEmpty()) {
                            throw new EmptyLineException("Empty line when title was parsing--> line in file: " + lineId);
                        }
                        category = lineReadyToUse;
                    } else {
                        rawCounter = 0; // reset the counter
                        // create and add a new book
                        books.add(new Book(author, title, category));
                    }
                    rawCounter++; // increase the raw counter
                } catch (EmptyLineException error) {
                    rawCounter = 1;
                    System.out.println(error.getMessage());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return books;
    }
}