import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Stats {

    public void countBooksPerCategory(List<Book> books) {
        Map<String, Integer> counterBookPerCategory = new HashMap<>();
        for (Book book : books) {
            if (!counterBookPerCategory.containsKey(book.getCategory())) {
                counterBookPerCategory.put(book.getCategory(), 1);
            } else {
                int counter = counterBookPerCategory.get(book.getCategory()) + 1;
                counterBookPerCategory.put(book.getCategory(), counter);
            }

        }

        System.out.println("--");
        System.out.println(counterBookPerCategory.keySet());
        //lack of "historia, polityka i publicystyka"
        System.out.println("--");

        for (String k : counterBookPerCategory.keySet()) {
            System.out.println("Ilość książek w kategori " + k + " wynosi " + counterBookPerCategory.get(k));
        }
    }

    public void topAuthors(List<Book> books) {
        Map<String, Integer> authorsStats = new HashMap<>();

        for (Book book : books) {
            int counter = authorsStats.getOrDefault(book.getAuthor(), 0) + 1;
            authorsStats.put(book.getAuthor(), counter);
        }

        for (String author : authorsStats.keySet()) {
            System.out.println(author + " -> " + authorsStats.get(author));
        }

    }

    public void topAuthorsInCategory(List<Book> books, String category) {
        Map<String, Integer> authorsStats = new HashMap<>();

        for (Book book : books) {
            if (book.getCategory().equals(category)) {
                int counter = authorsStats.getOrDefault(book.getAuthor(), 0) + 1;
                authorsStats.put(book.getAuthor(), counter);
            }
        }


        for (String author : authorsStats.keySet()) {
            System.out.println(author + " -> " + authorsStats.get(author));
        }
    }
}
