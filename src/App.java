import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

public class App {

    public static void main(String[] args) {

        try {
            String filePath = "./src/books-broken.txt";
            File fileToRead = new File(filePath);
            if (!fileToRead.exists()) {
                throw new FileNotFoundException();
            }

            Parser parser = new Parser();
            List<Book> books = parser.parse(filePath);

            // for testing purpose
            System.out.println(books.get(2));

            Stats stats = new Stats();
            stats.countBooksPerCategory(books);
            stats.topAuthors(books);
            stats.topAuthorsInCategory(books, "fantastyka");


        } catch (FileNotFoundException e) {
            System.out.println("Nie odnaleziono pliku, sprawdz czy nazwa pliku jest poprawna.");
        }
    }
}


