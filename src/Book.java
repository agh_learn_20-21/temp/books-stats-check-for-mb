import java.util.Objects;

public class Book {

    private final String author;
    private final String title;
    private final String category;

    public Book(String author, String title, String category) {
        if (author == null || title == null || category == null || author.isEmpty() || title.isEmpty() || category.isEmpty()) {
            throw new IllegalArgumentException("Książka zawiera nie kompletne dane");
        }
            this.author = author;
            this.title = title;
            this.category = category;

    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return author.equals(book.author) && title.equals(book.title) && category.equals(book.category);
    }

    public int hashCode() {
        return Objects.hash(author, title, category);
    }

    public String toString() {
        return "Book{" +
                "author='" + author + '\'' +
                ", title='" + title + '\'' +
                ", category='" + category + '\'' +
                '}';
    }

}
